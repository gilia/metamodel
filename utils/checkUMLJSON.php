<?php

// namespace Utils\UMLJSON;

function checkUMLJSON($uml_json){

    if(isset($uml_json['classes']) and isset($uml_json['links'])){
        $js_classes = $uml_json["classes"];
        $js_links = $uml_json["links"];

        foreach ($js_classes as $class){
            if(!isset($class['name']) or !isset($class['attrs'])){
                return false;
            }
        }

        foreach ($js_links as $link){
            
            if(!isset($link['name']) or !isset($link['classes']) or !isset($link['type'])){
                return false;
            }else{
                if(!array_key_exists('multiplicity', $link) or !array_key_exists('roles', $link)){
                    return false;
                }else{
                    if($link['type'] !== 'association' and $link['type'] !== 'generalization'){
                        print($link['type']);
                        return false;
                    }else{
                        if($link['type'] == 'generalization'){
                            if(!isset($link['constraint']) or isset($link['multiplicity']) or isset($link['roles'])){
                                return false;
                            }
                        }else{
                            if(!isset($link['multiplicity']) or !isset($link['roles']) ){
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }else{
        return false;
    }

    return true;
}


?>