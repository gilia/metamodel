<?php
declare(strict_types=1);

include 'import_functions.php';

load('checkUMLJSON.php', 'utils/');
// use function Utils\UMLJSON\checkUMLJSON;

use PHPUnit\Framework\TestCase;

final class checkUMLJSONTest extends TestCase
{
    public function testCorrectUMLJSON(): void
    {
        $uml_json_str = file_get_contents("tests/jsons/createModelTest/example_1.json");
        $uml_json = json_decode($uml_json_str, true);
        $this->assertEquals(true, checkUMLJSON($uml_json));
    }

    public function testNoClassesUMLJSON(): void
    {
        $uml_json_str = file_get_contents("tests/jsons/checkUMLJSONTest/no_classes.json");
        $uml_json = json_decode($uml_json_str, true);
        $this->assertEquals(false, checkUMLJSON($uml_json));
    }

    
    public function testNoLinksUMLJSON(): void
    {
        $uml_json_str = file_get_contents("tests/jsons/checkUMLJSONTest/no_links.json");
        $uml_json = json_decode($uml_json_str, true);
        $this->assertEquals(false, checkUMLJSON($uml_json));
    }

    public function testWrongClassContentUMLJSON(): void
    {
        $uml_json_str = file_get_contents("tests/jsons/checkUMLJSONTest/wrong_class_content.json");
        $uml_json = json_decode($uml_json_str, true);
        $this->assertEquals(false, checkUMLJSON($uml_json));
    }

    public function testWrongLinkAssociationContentUMLJSON(): void
    {
        $uml_json_str = file_get_contents("tests/jsons/checkUMLJSONTest/wrong_link_association_content.json");
        $uml_json = json_decode($uml_json_str, true);
        $this->assertEquals(false, checkUMLJSON($uml_json));
    }

    public function testWrongLinkGeneralizationContentUMLJSON(): void
    {
        $uml_json_str = file_get_contents("tests/jsons/checkUMLJSONTest/wrong_link_generalization_content.json");
        $uml_json = json_decode($uml_json_str, true);
        $this->assertEquals(false, checkUMLJSON($uml_json));
    }

}

?>