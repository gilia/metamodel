<?php
declare(strict_types=1);

include_once 'api/metamodelAPI.php';
use PHPUnit\Framework\TestCase;

final class MetamodelAPITest extends TestCase
{
    public function testCorrectUMLJSON(): void
    {
        $expected_json_str = file_get_contents("tests/jsons/metamodelAPI/example_1_response.json");

        $uml_json_str = file_get_contents("tests/jsons/metamodelAPI/example_1.json");

        $meta = new MetamodelAPI();
        $meta_str = $meta->getMetaFromUML($uml_json_str);
        $this->assertJsonStringEqualsJsonString(
            $expected_json_str,
            $meta_str
        );
    }

    public function testNoClassesUMLJSON(): void
    {
        $expected_json_str = file_get_contents("tests/jsons/metamodelAPI/error_response.json");

        $uml_json_str = file_get_contents("tests/jsons/metamodelAPI/no_classes.json");

        $meta = new MetamodelAPI();
        $meta_str = $meta->getMetaFromUML($uml_json_str);
        $this->assertJsonStringEqualsJsonString(
            $expected_json_str,
            $meta_str
        );
    }

    public function testNoLinksUMLJSON(): void
    {
        $expected_json_str = file_get_contents("tests/jsons/metamodelAPI/error_response.json");

        $uml_json_str = file_get_contents("tests/jsons/metamodelAPI/no_links.json");

        $meta = new MetamodelAPI();
        $meta_str = $meta->getMetaFromUML($uml_json_str);
        $this->assertJsonStringEqualsJsonString(
            $expected_json_str,
            $meta_str
        );
    }
 
}

?>