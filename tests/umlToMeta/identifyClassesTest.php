<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Wicom\Translator\MetaStrategies\UMLMeta;

// include 'metastrategies/umlmeta.php';

final class IdentifyClassesTest extends TestCase
{
    public function testUMLMetaCreatedCorrectly(): void
    {
        $umlMeta = new UMLMeta();
        $this->assertInstanceOf(
            UMLMeta::class,
            $umlMeta
        );
    }
    
    /**
     * @depends testUMLMetaCreatedCorrectly
     */
    public function testIdentifyClassesWithAttributes(): void
    {
        $expected_json_str = file_get_contents("tests/jsons/identifyClassesTest/classes_with_attributes_meta.json");
        $uml_json_str = file_get_contents("tests/jsons/identifyClassesTest/classes_with_attributes.json");
        $uml_json = json_decode($uml_json_str, true);
        $uml_meta = new UMLMeta();
        $uml_meta->identifyClasses($uml_json);
        $meta_json_str = $uml_meta->get_json();
        $this->assertJsonStringEqualsJsonString(
            $meta_json_str,
            $expected_json_str
        );
    }

    /**
     * @depends testUMLMetaCreatedCorrectly
     */
    public function testIdentifyClassesWithoutAttributes(): void
    {
        $expected_json_str = file_get_contents("tests/jsons/identifyClassesTest/classes_without_attributes_meta.json");
        $uml_json_str = file_get_contents("tests/jsons/identifyClassesTest/classes_without_attributes.json");
        $uml_json = json_decode($uml_json_str, true);
        $uml_meta = new UMLMeta();
        $uml_meta->identifyClasses($uml_json);
        $meta_json_str = $uml_meta->get_json();
        $this->assertJsonStringEqualsJsonString(
            $meta_json_str,
            $expected_json_str
        );
    }
 
}