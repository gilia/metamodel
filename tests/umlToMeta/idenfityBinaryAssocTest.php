<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Wicom\Translator\MetaStrategies\UMLMeta;

// include 'metastrategies/umlmeta.php';

final class idenfityBinaryAssocTest extends TestCase
{
    public function testUMLMetaCreatedCorrectly(): void
    {
        $umlMeta = new UMLMeta();
        $this->assertInstanceOf(
            UMLMeta::class,
            $umlMeta
        );
    }
    
    /**
     * @depends testUMLMetaCreatedCorrectly
     */
    public function testIdentifyBinaryAssoc(): void
    {
        $expected_json_str = file_get_contents("tests/jsons/identifyBinaryAssocTest/BinaryAssoc_meta.json");
        $uml_json_str = file_get_contents("tests/jsons/identifyBinaryAssocTest/BinaryAssoc.json");
        $uml_json = json_decode($uml_json_str, true);
        $uml_meta = new UMLMeta();
        $uml_meta->identifyBinaryAssoc($uml_json);
        $meta_json_str = $uml_meta->get_json();
        $this->assertJsonStringEqualsJsonString(
            $meta_json_str,
            $expected_json_str
        );
    }

    /**
     * @depends testUMLMetaCreatedCorrectly
     */
    // public function testIdentifyBinaryAssocWithCardinalities(): void
    // {
    //     $expected_json_str = file_get_contents("tests/jsons/identifyBinaryAssocTest/classes_with_attributes_meta.json");
    //     $uml_json_str = file_get_contents("tests/jsons/identifyBinaryAssocTest/classes_with_attributes.json");
    //     $uml_json = json_decode($uml_json_str, true);
    //     $uml_meta = new UMLMeta();
    //     $uml_meta->identifyBinaryAssoc($uml_json);
    //     $meta_json_str = $uml_meta->get_json();
    //     $this->assertJsonStringEqualsJsonString(
    //         $meta_json_str,
    //         $expected_json_str
    //     );
    // }

    /**
     * @depends testUMLMetaCreatedCorrectly
     */
    // public function testIdentifyBinaryAssocWithRoles(): void
    // {
    //     $expected_json_str = file_get_contents("tests/jsons/identifyBinaryAssocTest/classes_with_attributes_meta.json");
    //     $uml_json_str = file_get_contents("tests/jsons/identifyBinaryAssocTest/classes_with_attributes.json");
    //     $uml_json = json_decode($uml_json_str, true);
    //     $uml_meta = new UMLMeta();
    //     $uml_meta->identifyBinaryAssoc($uml_json);
    //     $meta_json_str = $uml_meta->get_json();
    //     $this->assertJsonStringEqualsJsonString(
    //         $meta_json_str,
    //         $expected_json_str
    //     );
    // }

    /**
     * @depends testUMLMetaCreatedCorrectly
     */
    // public function testIdentifyBinaryAssocWithCardinalitiesRoles(): void
    // {
    //     $expected_json_str = file_get_contents("tests/jsons/identifyBinaryAssocTest/classes_with_attributes_meta.json");
    //     $uml_json_str = file_get_contents("tests/jsons/identifyBinaryAssocTest/classes_with_attributes.json");
    //     $uml_json = json_decode($uml_json_str, true);
    //     $uml_meta = new UMLMeta();
    //     $uml_meta->identifyBinaryAssoc($uml_json);
    //     $meta_json_str = $uml_meta->get_json();
    //     $this->assertJsonStringEqualsJsonString(
    //         $meta_json_str,
    //         $expected_json_str
    //     );
    // }

}