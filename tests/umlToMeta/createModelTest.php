<?php
declare(strict_types=1);

include_once 'metastrategies/umlToMeta.php';

use PHPUnit\Framework\TestCase;
use Wicom\Translator\MetaStrategies\UMLMeta;



final class create_modelTest extends TestCase
{
    public function testUMLMetaCreatedCorrectly(): void
    {
        $umlMeta = new UMLMeta();
        $this->assertInstanceOf(
            UMLMeta::class,
            $umlMeta 
        );
    }
    
    /**
     * @depends testUMLMetaCreatedCorrectly
     */
    public function testCreateModel(): void
    {
        $expected_json_str = file_get_contents("tests/jsons/create_modelTest/example_1_meta.json");
        $uml_json_str = file_get_contents("tests/jsons/create_modelTest/example_1.json");
        $uml_json = $uml_json_str;//json_decode($uml_json_str, true);
        $uml_meta = new UMLMeta();
        $uml_meta->createModel($uml_json);
        $meta_json_str = $uml_meta->get_json();
        $this->assertJsonStringEqualsJsonString(
            $meta_json_str,
            $expected_json_str
        );
    }

}