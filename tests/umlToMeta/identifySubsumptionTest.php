<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Wicom\Translator\MetaStrategies\UMLMeta;

// include 'metastrategies/umlmeta.php';

final class IdentifySubsumptionTest extends TestCase
{
    public function testUMLMetaCreatedCorrectly(): void
    {
        $umlMeta = new UMLMeta();
        $this->assertInstanceOf(
            UMLMeta::class,
            $umlMeta
        );
    }
    
    ## NOTE: As long as Subsumption constraints are not implemented, all 4 possible cases will generate the same metamodel


    /**
     * @depends testUMLMetaCreatedCorrectly
     */
    public function testIdentifySubsumptionNoConstraints(): void
    {
        $expected_json_str = file_get_contents("tests/jsons/identifySubsumptionTest/generalization_meta.json");
        $uml_json_str = file_get_contents("tests/jsons/identifySubsumptionTest/generalization_no_constraints.json");
        $uml_json = json_decode($uml_json_str, true);
        $uml_meta = new UMLMeta();
        $uml_meta->identifySubsumption($uml_json);
        $meta_json_str = $uml_meta->get_json();
        $this->assertJsonStringEqualsJsonString(
            $meta_json_str,
            $expected_json_str
        );
    }

    /**
     * @depends testUMLMetaCreatedCorrectly
     */
    public function testIdentifySubsumptionDisjoint(): void
    {
        $expected_json_str = file_get_contents("tests/jsons/identifySubsumptionTest/generalization_meta.json");
        $uml_json_str = file_get_contents("tests/jsons/identifySubsumptionTest/generalization_disjoint.json");
        $uml_json = json_decode($uml_json_str, true);
        $uml_meta = new UMLMeta();
        $uml_meta->identifySubsumption($uml_json);
        $meta_json_str = $uml_meta->get_json();
        $this->assertJsonStringEqualsJsonString(
            $meta_json_str,
            $expected_json_str
        );
    }

    /**
     * @depends testUMLMetaCreatedCorrectly
     */
    public function testIdentifySubsumptionCovering(): void
    {
        $expected_json_str = file_get_contents("tests/jsons/identifySubsumptionTest/generalization_meta.json");
        $uml_json_str = file_get_contents("tests/jsons/identifySubsumptionTest/generalization_covering.json");
        $uml_json = json_decode($uml_json_str, true);
        $uml_meta = new UMLMeta();
        $uml_meta->identifySubsumption($uml_json);
        $meta_json_str = $uml_meta->get_json();
        $this->assertJsonStringEqualsJsonString(
            $meta_json_str,
            $expected_json_str
        );
    }
    
    /**
     * @depends testUMLMetaCreatedCorrectly
     */
    public function testIdentifySubsumptionCoveringDisjoint(): void
    {
        $expected_json_str = file_get_contents("tests/jsons/identifySubsumptionTest/generalization_meta.json");
        $uml_json_str = file_get_contents("tests/jsons/identifySubsumptionTest/generalization_covering_and_disjoint.json");
        $uml_json = json_decode($uml_json_str, true);
        $uml_meta = new UMLMeta();
        $uml_meta->identifySubsumption($uml_json);
        $meta_json_str = $uml_meta->get_json();
        $this->assertJsonStringEqualsJsonString(
            $meta_json_str,
            $expected_json_str
        );
    }    

}