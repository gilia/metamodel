<?php

//include_once 'utils/checkUMLJSON.php';
// include_once 'metastrategies/umlToMeta.php';
// include_once '../metastrategies/metaToUML.php';
require_once 'import_functions.php';
load('umlToMeta.php', 'metastrategies/');
load('checkUMLJSON.php', 'utils/');
use Wicom\Translator\MetaStrategies\UMLMeta;

class MetamodelAPI {

        function getMetaFromUML($uml_json_str){

            $uml_json = json_decode($uml_json_str, true); // Decode JSON String to JSON

            if(json_last_error() === JSON_ERROR_NONE){
                if(checkUMLJSON($uml_json)){
                    $uml_meta = new UMLMeta();
                    $uml_meta->createModel($uml_json_str);
                    $meta_json_str = $uml_meta->get_json();
                }else{
                    $error = "Wrong UML JSON format";
                    $success = false;
                    $meta_json_str = json_encode(array('success' => $success, 'error' => $error));;
                }
            }else{
                $error = "Could not decode JSON String";
                $success = false;
                $meta_json_str = json_encode(array('success' => $success, 'error' => $error));;
            }

            
            return $meta_json_str;
        }

        function getUMLFromMeta($meta_json){
            $meta_uml = new MetaUML();
            $meta_uml->createModel($meta_json);
            $uml_json_str = $meta_uml->get_json();

            echo $uml_json_str;
        }


}





?>