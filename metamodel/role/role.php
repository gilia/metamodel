<?php
/*

Copyright 2017

Grupo de Investigación en Lenguajes e Inteligencia Artificial (GILIA) -
Facultad de Informática
Universidad Nacional del Comahue

role.php

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Wicom\Translator\Metamodel;
use function \load;
load('../entity.php');

	// TODO: Terminar las equivalencias de Role
	class Role extends Entity{

		protected $objectRelName = "";
		protected $objectRoleName = "";

		/**
		 UML: Association end/member end
		 EER: Component of a relationship
		 ORM: Role
		 
		 * @param $relname relationship name to which the role is associated
		 * @param $rolename role name
		 */

		function __construct($relname,$rolename){
		
			$this->objectRelName = $relname;
			$this->objectRoleName = $rolename;
		
		}

		function get_json_array(){
			return ["relationship" => $this->objectRelName,
					"rol" => $this->objectRoleName,
			];
		
		}
		
		
	}

?>